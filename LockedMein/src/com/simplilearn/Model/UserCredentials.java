package com.simplilearn.Model;

public class UserCredentials {
	
	private String siteName;
	private String loggedUser;
	private String userNAme;
	private String password;
	
	public UserCredentials() {}

	public UserCredentials(String siteName, String loggedUser, String userNAme, String password) {
		this.siteName = siteName;
		this.loggedUser = loggedUser;
		this.userNAme = userNAme;
		this.password = password;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(String loggedUser) {
		this.loggedUser = loggedUser;
	}

	public String getUserNAme() {
		return userNAme;
	}

	public void setUserNAme(String userNAme) {
		this.userNAme = userNAme;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserCredentials [siteName=" + siteName + ", loggedUser=" + loggedUser + ", userNAme=" + userNAme
				+ ", password=" + password + "]";
	}

	
	
	

}
